require essioc
require adsis8300bpm

# set macros
epicsEnvSet("CONTROL_GROUP",  "PBI-BPM12")
epicsEnvSet("AMC_NAME",       "Ctrl-AMC-120")
epicsEnvSet("AMC_DEVICE",     "/dev/sis8300-5")
epicsEnvSet("EVR_NAME",       "PBI-BPM12:Ctrl-EVR-101:")
epicsEnvSet("SYSTEM1_PREFIX", "HEBT-070LWU:PBI-BPM-001:")
epicsEnvSet("SYSTEM1_NAME",   "HEBT 070 LWU BPM 01")
epicsEnvSet("SYSTEM2_PREFIX", "HEBT-080LWU:PBI-BPM-001:")
epicsEnvSet("SYSTEM2_NAME",   "HEBT 080 LWU BPM 01")

# load BPM
iocshLoad("$(adsis8300bpm_DIR)/bpm-main.iocsh", "CONTROL_GROUP=$(CONTROL_GROUP), AMC_NAME=$(AMC_NAME), AMC_DEVICE=$(AMC_DEVICE), EVR_NAME=$(EVR_NAME)")

# load common module
iocshLoad $(essioc_DIR)/common_config.iocsh

